#!/usr/env/bin python3

from prettytable import PrettyTable
from colorama import Fore, Style
from datetime import datetime
from dateutil import tz
import requests
import json


IGNORED_CURRENCIES = ["CJM", "CTT", "SLVR", "GOLD"]
UTC_TIMEZONE = tz.gettz("UTC")
LY_TIMEZONE = tz.gettz("Africa/Libya")
ESSALE_API = "https://essale.ly/api"


def get_currency_list() -> list:
	endpoint = ESSALE_API + "/tick"
	response = requests.get(url=endpoint)
	json_text = response.text
	json_data = json.loads(json_text)

	return json_data


def get_formated_date_string(date_string: str) -> str:
	date = datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%fZ")
	utc_date = date.replace(tzinfo=UTC_TIMEZONE)
	ly_date = utc_date.astimezone(tz=LY_TIMEZONE)
	formated_ly_date = ly_date.strftime(format="Last Update: [%b %d, %H:%M]\n")

	return formated_ly_date


def get_chnage_percentage_string(change_percentage: int) -> str:
	if change_percentage > 0:
		icon = "↑"
		color = Fore.GREEN
	elif change_percentage < 0:
		icon = "↓"
		color = Fore.RED
	else:
		icon = "—"
		color = Fore.WHITE

	cp_text = color + str("{:.2f}".format(abs(change_percentage))) + "% " + icon + Style.RESET_ALL

	return cp_text


def get_table_object(currency_list: list, ignored_currencies: list = []) -> PrettyTable:
	table = PrettyTable()
	table.field_names = ["Currency", "Price", "%"]

	for currency_dict in currency_list:
		currency_name = currency_dict["n"]
		
		if currency_name in ignored_currencies:
			continue

		currency_price = str("{:.2f}".format(currency_dict["v"])) + " LYD"
		price_change_percentage = get_chnage_percentage_string(currency_dict["cp"])

		table.add_row([currency_name, currency_price, price_change_percentage])

	return table


def get_table_with_date(ignored_currencies: list = []) -> str:
	currency_list = get_currency_list()
	currency_table = str(get_table_object(currency_list, ignored_currencies))

	prices_update_text = get_formated_date_string(currency_list[0]["d"])

	response_message = prices_update_text + currency_table + "\n"

	return response_message



if __name__ == "__main__":
	print(get_table_with_date(ignored_currencies=IGNORED_CURRENCIES))
