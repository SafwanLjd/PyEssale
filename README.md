# PyEssale

This is a simple python script that interacts with [Essale's API](https://essale.ly) to fetch currency prices and display them in a presentable way.
